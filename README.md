# openshift-audit-query

A small script to run a `jq` script against all available audit logs on all
OpenShift master nodes



## Examples
1. Find out who deleted _anything_ in the openshift-logging namespace, but not
   by a service account:
   ```bash
   openshift-audit-query '.[] | select(.objectRef.namespace=="openshift-logging")
                              | select(.verb == "delete")
                              | select(.user.username | startswith("system:") | not)'
   ```
2. Figure out who created, updated, or patched the secret `htpass-secret` in the `openshift-config` namespace:
   ```bash
   openshift-audit-query '.[] | select(.objectRef.namespace=="openshift-config" and
                                       .objectRef.name=="htpass-secret" and
                                       .objectRef.resource=="secrets" and
                                       .objectRef.apiVersion=="v1" and
                                       (.verb | IN(["create","update","patch"][])))'
   ```
   *N.B.* The `jq` `IN` functions requires `jq` 1.6 or higher; RHEL8 ships with 1.5.x
